// const mongoose = require('mongoose');

// const Schema = mongoose.Schema;
// //creamos nuevo
// const userSchema = new Schema({
//         //email y password 
//         email: { type: String, require: true },
//         password: { type: String, require: true },
//         name: { type: String }
//     },

//     { timestamps: true }
// );

// // lo guardo en user
// const User = mongoose.model('User', userSchema);

// module.exports = User;

///////////////////////////////////////////////////


const mongoose = require('mongoose');

const Schema = mongoose.Schema;

//vamos a crear nuestro esquema
const userSchema = new Schema({
    email: { type: String, required: true },
    password: { type: String, required: true },
    name: { type: String }
}, { timestamps: true })

const User = mongoose.model('User', userSchema);

//Exportamos
module.exports = User;
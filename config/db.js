const mongoose = require('mongoose');

// creamos nombre DB
const DB_URL = 'mongodb://localhost:27017/autentication';

//guardamos en una const la configuración estandar de la DB
const CONFIG_DB = { useNewUrlParser: true, useUnifiedTopology: true };

// función asíncrona de conexión
const connectToDb = async() => {
    try {
        //conectamos con response a mongoose con la Url y la config 
        //antes fijadas
        const response = await mongoose.connect(DB_URL, CONFIG_DB);
        //destructuramos response para sacar host, port, name
        const { host, port, name } = response.connection;
        //pintamos los datos antes obtenidos por destructuring
        console.log(`Conectado a ${name} en ${host}:${port}`);
    } catch (error) {
        console.log("Conexión con DB a tomar por culo", error);
    }
}

//exportamos para usar en otros documentos
module.exports = {
    DB_URL,
    CONFIG_DB,
    connectToDb,
}
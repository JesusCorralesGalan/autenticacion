const passport = require('passport'); //requerimos passport
const LocalStrategy = require('passport-local').Strategy; //requerimos passport-local: la estrategia
const User = require('../models/User');
const bcrypt = require('bcrypt');

// funcion para ver que el mail es válido
const validateEmail = (email) => {
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLocaleLowerCase()); // esta expresion pasa el email por el test de re (el codigazo de arriba)
    // si el test sale bien te devuelve un booleano: true
};

//funcion para comprobar que el password es valido exactamente igual que arriba
const validatePassword = (password) => {
    const re = /^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/;
    return re.test(String(password));
};

//función para registrar
const registerStrategy = new LocalStrategy({
        usernameField: 'email', // cual es el campo que contiene usuario? el email en el modelo
        passwordField: 'password', // cual es el campo que contiene contraseña? el password en el modelo
        passReqToCallback: true, // ejecutamos el callback justo abajo (linea....)
    },


    //en el primer argumento pasamos la request
    //en el callback pasa como segundo argumento usernameField
    //en el callback pasa como tercer argumento passwordField
    // el cuarto argumento es una función que hace las veces next en el error
    async(req, email, password, done) => {


        //1.- Comprobamos que el usuario existe (que el usuario email existe en email)
        try {
            //ejecutamos la funcion de validacion de email
            const isValidEmail = validateEmail(email);
            if (!isValidEmail) {
                const error = new Error('email no valido, no te cueles');
                done(error);
            }


            //ejecutamos la funcion de validacion de contraseña
            const isValidPassword = validatePassword(password);
            if (!isValidPassword) {
                const error = new Error('de 6 a 16 caracteres, una mayuscula, una minuscula y un numero, algo te falta');
                done(error);
            }

            const existingUser = await User.find({
                email: email
            }); // email entra como parametro
            if (existingUser.length) {
                const error = new Error('El usuario ya estaba');
                //error.status = 401;
                return done(error);
            }
            //1.1 comprobamos que es un mail válido

            //2.- Encriptar la contraseña y guardarla en una variable
            const saltRounds = 10; //encripta la contraseña hasta 10 veces
            const hash = await bcrypt.hash(password, saltRounds);

            //3.- Creamos el usuario y lo guardamos en db
            const newUser = new User({
                email: email,
                password: hash,
                name: req.body.name, // está definido en  línea 10
            });
            const savedUser = await newUser.save(); // lo salvamos

            return done(null, savedUser);


        } catch (error) {
            done(error, null);
        }
        //4.- Le iniciamos la sesión al usuario y llamando a passport.authenticate();
    }
);

passport.use('registrazo', registerStrategy);

module.exports = registerStrategy; // exportamos la funcion de registro de usuario
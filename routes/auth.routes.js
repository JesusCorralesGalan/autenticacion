const express = require('express');
const User = require('../models/User');
const passport = require('passport'); //requerimos passport

const router = express.Router();

router.post('/regist', (req, res, next) => {
    //return res.status(200. json('registrando por ahora'));
    try { //esta función irá como cuarto argumento en la estrategia
        //register.strategy.js / linea 16
        const done = (error, savedUser) => {
            if (error) {
                return next(error);
            }

            return res.status(201).json(savedUser);
        };
        //función propia de passport que te pasa como primer argumento
        //el nombre de la estrategia (en register.strategy)
        // segundo argumento el done (línea 8)
        passport.authenticate('registrazo', done)(req); //no entiendo el req
    } catch (error) {
        return next(error)
    }
});

// router.post('/register', (req, res, next) => {
//     // Invocamos a la autenticación de Passport
//     passport.authenticate('register', (error, savedUser) => {
//         // Si hay un error, llamamos a nuestro controlador de errores
//         if (error) {
//             return next(error);
//         }

//         // Si no hay error, devolvemos el user registrado
//         return res.status(201).json(savedUser)
//     })(req); // ¡No te olvides de invocarlo aquí!
// });


module.exports = router;
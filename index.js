const express = require('express');
//mediante destructuring del objeto export obtenemos la función connectToDb de config/db.js
const { connectToDb } = require('./config/db');


const passport = require('passport'); //requerimos passport
require('./auth/register.strategy');

const PORT = 3000;


const authRouter = require('./routes/auth.routes');

connectToDb(); //invocamos connectToDb y, pues eso, nos conectamos a la db.
const server = express();

//CONFIGURAMOS EXPRESS PARA QUE FUNCIONE POST
// Esta línea, le dice a Express, que si recibe un (POST, PUT...) con un JSON adjunto, lo lea
// y nos lo mande al endpoint dentro de req.body;
server.use(express.json());
// Si la petición (POST, PUT.. ) y viene en formato form-urlencoded, meta la información en req.body;
server.use(express.urlencoded({ extended: false }));
//inicializamos  passport
server.use(passport.initialize());

// Controlador de Errores (error handler)
// Única función de express que lleva como primer argumento un error.
server.use((error, req, res, next) => {

    const status = error.status || 500;
    const message = error.message || 'Te acaba de saltar el error handler grijander morenaguër'; //este mensaje no me aparece y no sé why??

    return res.status(status).json(message);
});



server.use('/auth', authRouter);



server.listen(PORT, () => {
    console.log(`Server a tope de powa en http://localhost:${PORT}`);
});